﻿namespace sisExportImport
{
    partial class FormExportPurchaseOrders
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExpot = new System.Windows.Forms.Button();
            this.btnSimpan = new System.Windows.Forms.Button();
            this.txtPurchaseOrders = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnExpot
            // 
            this.btnExpot.Location = new System.Drawing.Point(12, 12);
            this.btnExpot.Name = "btnExpot";
            this.btnExpot.Size = new System.Drawing.Size(75, 23);
            this.btnExpot.TabIndex = 0;
            this.btnExpot.Text = "Export";
            this.btnExpot.UseVisualStyleBackColor = true;
            this.btnExpot.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnSimpan
            // 
            this.btnSimpan.Location = new System.Drawing.Point(359, 39);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(75, 23);
            this.btnSimpan.TabIndex = 1;
            this.btnSimpan.Text = "Simpan";
            this.btnSimpan.UseVisualStyleBackColor = true;
            this.btnSimpan.Click += new System.EventHandler(this.btnSimpan_Click);
            // 
            // txtPurchaseOrders
            // 
            this.txtPurchaseOrders.Location = new System.Drawing.Point(12, 41);
            this.txtPurchaseOrders.Name = "txtPurchaseOrders";
            this.txtPurchaseOrders.Size = new System.Drawing.Size(328, 20);
            this.txtPurchaseOrders.TabIndex = 2;
            // 
            // FormExportPurchaseOrders
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(462, 83);
            this.Controls.Add(this.txtPurchaseOrders);
            this.Controls.Add(this.btnSimpan);
            this.Controls.Add(this.btnExpot);
            this.Name = "FormExportPurchaseOrders";
            this.Text = "FormExportPurchaseOrders";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnExpot;
        private System.Windows.Forms.Button btnSimpan;
        private System.Windows.Forms.TextBox txtPurchaseOrders;
    }
}