﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sisExportImport.IMPORTEKSPORTAPI.MODELS
{
    class purchase_orders
    {
        public Guid idPurchaseOrder { get; set; }
        public string noPurchaseOrder { get; set; }
        public DateTime? dtOrder { get; set; }
        public string divition { get; set; }
        public string sifat { get; set; }
        public string needs { get; set; }
        public string appPembelian { get; set; }
        public DateTime? dtAppPembelian { get; set; }
        public string appPabrik { get; set; }
        public DateTime? dtAppPabrik { get; set; }
        public string appDept { get; set; }
        public DateTime? dtAppDept { get; set; }
        public string createdBy { get; set; }
        public string tipe { get; set; }
        public DateTime? dtCreateBy { get; set; }
        public string ket { get; set; }
        public int Totaldata { get; set; }
        public DateTime? dtNecessity { get; set; }
        public int? idStatusType { get; set; }
        public string status { get; set; }
        public string contentContentType { get; set; }
        public byte[] content { get; set; }
        public string pic { get; set; }
        public DateTime? dtApproved { get; set; }
        public string diusulkan { get; set; }
        public string app1 { get; set; }
        public DateTime? dtapp1 { get; set; }
        public string app2 { get; set; }
        public DateTime? dtapp2 { get; set; }
        public DateTime? dtappspv { get; set; }
        public DateTime? dtappmgr { get; set; }
        public DateTime? dtapppbrk { get; set; }
        public List<PurchaseOrderItemVModel> data { get; set; }
        public List<purchase_orders_status> statuses { get; set; }

        public class PurchaseOrderItemVModel
        {
            public Guid idPurOrdItem { get; set; }
            public Guid idPurchaseOrder { get; set; }
            public string codeProduk { get; set; }
            public string produkName { get; set; }
            public Double? qty { get; set; }
            public string unit { get; set; }
            public DateTime? dtOffer { get; set; }
            public string offerNumber { get; set; }
            public string supplyerName { get; set; }
            public int? supplyerPrice { get; set; }
            public string necessity { get; set; }
            public DateTime? dtNecessity { get; set; }
            public string remainingStock { get; set; }
            public string remainingPo { get; set; }
            public DateTime? dtCreate { get; set; }
            public int Totaldata { get; set; }
            public string catatan { get; set; }
            public int? status { get; set; }
            public string mataUang { get; set; }
            public string pic { get; set; }
            public int? noUrut { get; set; }
            public Double? qty_terpenuhi { get; set; }
            public string satuan { get; set; }

        }

        public partial class purchase_orders_status
        {
            public Guid idstatus { get; set; }
            public Guid idpurchaseorder { get; set; }
            public int? statustype { get; set; }
            public DateTime dtfrom { get; set; }
            public DateTime dtthru { get; set; }
            public string author { get; set; }

            public virtual purchase_orders purchase_orders { get; set; }
        }
    }
}
