﻿namespace sisExportImport
{
    partial class MainPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.importPurchasingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportPurchasingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importPurchaseOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportPurchaseOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(719, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importPurchasingToolStripMenuItem,
            this.exportPurchasingToolStripMenuItem,
            this.importPurchaseOrderToolStripMenuItem,
            this.exportPurchaseOrderToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(50, 20);
            this.toolStripMenuItem1.Text = "Menu";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // importPurchasingToolStripMenuItem
            // 
            this.importPurchasingToolStripMenuItem.Name = "importPurchasingToolStripMenuItem";
            this.importPurchasingToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.importPurchasingToolStripMenuItem.Text = "Import Purchasing";
            this.importPurchasingToolStripMenuItem.Click += new System.EventHandler(this.importPurchasingToolStripMenuItem_Click);
            // 
            // exportPurchasingToolStripMenuItem
            // 
            this.exportPurchasingToolStripMenuItem.Name = "exportPurchasingToolStripMenuItem";
            this.exportPurchasingToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.exportPurchasingToolStripMenuItem.Text = "Export Purchasing";
            this.exportPurchasingToolStripMenuItem.Click += new System.EventHandler(this.exportPurchasingToolStripMenuItem_Click);
            // 
            // importPurchaseOrderToolStripMenuItem
            // 
            this.importPurchaseOrderToolStripMenuItem.Name = "importPurchaseOrderToolStripMenuItem";
            this.importPurchaseOrderToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.importPurchaseOrderToolStripMenuItem.Text = "Import Purchase Order";
            this.importPurchaseOrderToolStripMenuItem.Click += new System.EventHandler(this.importPurchaseOrderToolStripMenuItem_Click);
            // 
            // exportPurchaseOrderToolStripMenuItem
            // 
            this.exportPurchaseOrderToolStripMenuItem.Name = "exportPurchaseOrderToolStripMenuItem";
            this.exportPurchaseOrderToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.exportPurchaseOrderToolStripMenuItem.Text = "Export Purchase Order";
            this.exportPurchaseOrderToolStripMenuItem.Click += new System.EventHandler(this.exportPurchaseOrderToolStripMenuItem_Click);
            // 
            // MainPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(719, 398);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainPage";
            this.Text = "MainPage";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem importPurchasingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportPurchasingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importPurchaseOrderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportPurchaseOrderToolStripMenuItem;
    }
}