﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using sisExportImport.IMPORTEKSPORTAPI.Helper;
using Newtonsoft.Json;
using sisExportImport.IMPORTEKSPORTAPI.MODELS;
using System.Net;

namespace sisExportImport
{
    public partial class FormExportPurchaseOrders : Form
    {
        string pathPurchaseOrderItem = "";
        string pathPurchaseOrdersstatus = "";
        public FormExportPurchaseOrders()
        {
            InitializeComponent();
        }

        public string webGetMethod(string URL)
        {
            string jsonString = "";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
            request.Method = "GET";
            request.Credentials = CredentialCache.DefaultCredentials;
            ((HttpWebRequest)request).UserAgent = "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 7.1; Trident/5.0)";
            request.Accept = "/";
            request.UseDefaultCredentials = true;
            request.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
            request.ContentType = "application/x-www-form-urlencoded";

            WebResponse response = request.GetResponse();
            StreamReader sr = new StreamReader(response.GetResponseStream());
            jsonString = sr.ReadToEnd();
            sr.Close();
            return jsonString;
        }

        public void getDataJson()
        {
            var url = "http://localhost:3908/api/ekspor/pp";
            var getData = webGetMethod(url);
            List<purchase_orders> pps = JsonConvert.DeserializeObject<List<purchase_orders>>(getData);
            foreach (var pp in pps)
            {
                MessageBox.Show("dataaa", pp.noPurchaseOrder);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //getDataJson();
            exportPurchaseOrders();
            exportPurchaseOrderItem();
            exportPurchaseOrdersStatus();
            MessageBox.Show("selesai");
        }

        public void exportPurchaseOrders()
        {
            //OpenSqlConnection();
            dbConnection dbCon = new dbConnection();
            SqlConnection con = new SqlConnection(dbCon.GetConnectionString());
            SqlCommand command = con.CreateCommand();

            command.CommandText = @"select a.* from purchase_orders a
                                    join purchase_orders_status b on a.idpurchaseorder = b.idpurchaseorder
                                    where b.statustype = 21 and b.dtfrom < GETDATE() and GETDATE() < b.dtthru";
            con.Open();
            SqlDataReader queryReader = command.ExecuteReader();
            StreamWriter file = new StreamWriter(txtPurchaseOrders.Text);

            bool addColumns = false;
            string idpurchaseorder = "idpurchaseorder";
            string nopurchaseorder = "nopurchaseorder";
            string dtorder = "dtorder";
            string divition = "divition";
            string sifat = "sifat";
            string needs = "needs";
            string apppembelian = "apppembelian";
            string dtapppembelian = "dtapppembelian";
            string apppabrik = "apppabrik";
            string dtapppabrik = "dtapppabrik";
            string appdept = "appdept";
            string dtappdept = "dtappdept";
            string createby = "createby";
            string dtcreateby = "dtcreateby";
            string tipe = "tipe";
            string no_bbm = "no_bbm";
            string ket = "ket";
            string dtnecessity = "dtnecessity";
            string contenttype = "contenttype";
            string content = "content";
            string diusulkan = "diusulkan";
            string app1 = "app1";
            string dtapp1 = "dtapp1";
            string app2 = "app2";
            string dtapp2 = "dtapp2";

            while (queryReader.Read())
            {
                if (addColumns)
                {
                    file.WriteLine(idpurchaseorder + "|" +
                                    nopurchaseorder + "|" +
                                    dtorder + "|" +
                                    divition + "|" +
                                    sifat + "|" +
                                    needs + "|" +
                                    apppembelian + "|" +
                                    dtapppembelian + "|" +
                                    apppabrik + "|" +
                                    dtapppabrik + "|" +
                                    appdept + "|" +
                                    dtappdept + "|" +
                                    createby + "|" +
                                    dtcreateby + "|" +
                                    tipe + "|" +
                                    no_bbm + "|" +
                                    ket + "|" +
                                    dtnecessity + "|" +
                                    contenttype + "|" +
                                    content + "|" +
                                    diusulkan + "|" +
                                    app1 + "|" +
                                    dtapp1 + "|" +
                                    app2 + "|" +
                                    dtapp2);
                    addColumns = true;
                }
                else
                {
                    string CdtOrder = queryReader["dtorder"] == DBNull.Value ? null : Convert.ToDateTime(queryReader["dtorder"]).ToString("yyyy-MM-dd HH:mm:ss");
                    string CdtAppPembelian = queryReader["dtapppembelian"] == DBNull.Value ? null:Convert.ToDateTime(queryReader["dtapppembelian"]).ToString("yyyy-MM-dd HH:mm:ss");
                    string CdtAppPabrik = queryReader["dtapppabrik"] == DBNull.Value ? null : Convert.ToDateTime(queryReader["dtapppabrik"]).ToString("yyyy-MM-dd HH:mm:ss");
                    string CdtAppDept = queryReader["dtappdept"] == DBNull.Value ? null : Convert.ToDateTime(queryReader["dtappdept"]).ToString("yyyy-MM-dd HH:mm:ss");
                    string CdtCreateby = queryReader["dtcreateby"] == DBNull.Value ? null : Convert.ToDateTime(queryReader["dtcreateby"]).ToString("yyyy-MM-dd HH:mm:ss");
                    string CdtNecesary = queryReader["dtnecessity"] == DBNull.Value ? null : Convert.ToDateTime(queryReader["dtnecessity"]).ToString("yyyy-MM-dd HH:mm:ss");
                    string CdtAPP1 = queryReader["dtapp1"] == DBNull.Value ? null : Convert.ToDateTime(queryReader["dtapp1"]).ToString("yyyy-MM-dd HH:mm:ss");
                    string cDtAPP2 = queryReader["dtapp2"] == DBNull.Value ? null : Convert.ToDateTime(queryReader["dtapp2"]).ToString("yyyy-MM-dd HH:mm:ss");
                    string Ccontent =  queryReader["content"] == DBNull.Value ? "0x" : queryReader["content"].ToString();
                    file.WriteLine(queryReader["idpurchaseorder"].ToString() + "|" +
                                    queryReader["nopurchaseorder"].ToString() + "|" +
                                    CdtOrder + "|" +
                                    queryReader["divition"].ToString() + "|" +
                                    queryReader["sifat"].ToString() + "|" +
                                    queryReader["needs"].ToString() + "|" +
                                    queryReader["apppembelian"].ToString() + "|" +
                                    CdtAppPembelian + "|" +
                                    queryReader["apppabrik"].ToString() + "|" +
                                    CdtAppPabrik + "|" +
                                    queryReader["appdept"].ToString() + "|" +
                                    CdtAppDept + "|" +
                                    queryReader["createby"].ToString() + "|" +
                                    CdtCreateby + "|" +
                                    queryReader["tipe"].ToString() + "|" +
                                    queryReader["no_bbm"].ToString() + "|" +
                                    queryReader["ket"].ToString() + "|" +
                                    CdtNecesary + "|" +
                                    queryReader["contenttype"].ToString() + "|" +
                                    Ccontent + "|" +
                                    queryReader["diusulkan"].ToString() + "|" +
                                    queryReader["app1"].ToString() + "|" +
                                    CdtAPP1 + "|" +
                                    queryReader["app2"].ToString() + "|" +
                                    cDtAPP2);
                }
            }

            queryReader.Close();
            con.Close();
            file.Close();
        }

        public void exportPurchaseOrderItem()
        {
            //OpenSqlConnection();
            dbConnection dbCon = new dbConnection();
            SqlConnection con = new SqlConnection(dbCon.GetConnectionString());
            SqlCommand command = con.CreateCommand();

            command.CommandText = @"select c.* from purchase_orders a
                                    join purchase_orders_status b on a.idpurchaseorder = b.idpurchaseorder
                                    join purchase_order_item c on a.idpurchaseorder = c.idpurchaseorder
                                    where b.statustype = 21 and b.dtfrom < GETDATE() and GETDATE() < b.dtthru";
            con.Open();
            SqlDataReader queryReader = command.ExecuteReader();
            StreamWriter file = new StreamWriter(pathPurchaseOrderItem);

            bool addColumns = false;
            string idpurorditem = "idpurorditem";
            string idpurchaseorder = "idpurchaseorder";
            string codeproduk = "codeproduk";
            string produkname = "produkname";
            string qty = "qty";
            string unit = "unit";
            string dtoffer = "dtoffer";
            string supplyername = "supplyername";
            string supplyerprice = "supplyerprice";
            string necessity = "necessity";
            string dtnecessity = "dtnecessity";
            string remainingstock = "remainingstock";
            string remainingpo = "remainingpo";
            string dtcreate = "dtcreate";
            string no_ph = "no_ph";
            string catatan = "catatan";
            string status = "status";
            string mata_uang = "mata_uang";
            string qtybookingpo = "qtybookingpo";
            //string kd_lama = "kd_lama";
            string no_urut = "no_urut";
            string qty_terpenuhi = "qty_terpenuhi";

            while (queryReader.Read())
            {
                if (addColumns)
                {
                    file.WriteLine(idpurchaseorder + "|" +
                                    idpurorditem + "|" +
                                    idpurchaseorder + "|" +
                                    codeproduk + "|" +
                                    produkname + "|" +
                                    qty + "|" +
                                    unit + "|" +
                                    dtoffer + "|" +
                                    supplyername + "|" +
                                    supplyerprice + "|" +
                                    necessity + "|" +
                                    dtnecessity + "|" +
                                    remainingstock + "|" +
                                    remainingpo + "|" +
                                    dtcreate + "|" +
                                    no_ph + "|" +
                                    catatan + "|" +
                                    status + "|" +
                                    mata_uang + "|" +
                                    qtybookingpo + "|" +
                                    //kd_lama + "|" +
                                    no_urut + "|" +
                                    qty_terpenuhi);
                    addColumns = true;
                }
                else
                {
                    string CdtOffer = queryReader["dtoffer"] == DBNull.Value ? null : Convert.ToDateTime(queryReader["dtoffer"]).ToString("yyyy-MM-dd HH:mm:ss");
                    string CdtNecesity =     queryReader["dtnecessity"] == DBNull.Value ? null : Convert.ToDateTime(queryReader["dtnecessity"]).ToString("yyyy-MM-dd HH:mm:ss");
                    string CdtCreate =     queryReader["dtcreate"] == DBNull.Value ? null : Convert.ToDateTime(queryReader["dtcreate"]).ToString("yyyy-MM-dd HH:mm:ss");
                    //file.WriteLine(queryReader["idpurchasing"].ToString() + "|" + queryReader["nopurchasing"].ToString());
                    file.WriteLine(queryReader["idpurorditem"].ToString() + "|" +
                                    queryReader["idpurchaseorder"].ToString() + "|" +
                                    queryReader["codeproduk"].ToString() + "|" +
                                    queryReader["produkname"].ToString() + "|" +
                                    queryReader["qty"].ToString() + "|" +
                                    queryReader["unit"].ToString() + "|" +
                                    CdtOffer + "|" +
                                    queryReader["supplyername"].ToString() + "|" +
                                    queryReader["supplyerprice"].ToString() + "|" +
                                    queryReader["necessity"].ToString() + "|" +
                                    CdtNecesity + "|" +
                                    queryReader["remainingstock"].ToString() + "|" +
                                    queryReader["remainingpo"].ToString() + "|" +
                                    CdtCreate + "|" +
                                    queryReader["no_ph"].ToString() + "|" +
                                    queryReader["catatan"].ToString() + "|" +
                                    queryReader["status"].ToString() + "|" +
                                    queryReader["mata_uang"].ToString() + "|" +
                                    queryReader["qtybookingpo"].ToString() + "|" +
                                    //queryReader["kd_lama"].ToString() + "|" +
                                    queryReader["no_urut"].ToString() + "|" +
                                    queryReader["qty_terpenuhi"].ToString());
                }
            }

            queryReader.Close();
            con.Close();
            file.Close();
        }

        public void exportPurchaseOrdersStatus()
        {
            //OpenSqlConnection();
            dbConnection dbCon = new dbConnection();
            string connString = dbCon.GetConnectionString();
            SqlConnection con = new SqlConnection(connString);
            SqlCommand command = con.CreateCommand();

            command.CommandText = @"select  * from purchase_orders_status where idpurchaseorder in (
                                            select a.idpurchaseorder from purchase_orders a
                                            join purchase_orders_status b on a.idpurchaseorder = b.idpurchaseorder
                                            where b.statustype = 21 and b.dtfrom < GETDATE() and GETDATE() < b.dtthru)";
            con.Open();
            SqlDataReader queryReader = command.ExecuteReader();
            StreamWriter file = new StreamWriter(pathPurchaseOrdersstatus);

            bool addColumns = false;
            string idstatus = "idstatus";
            string idpurchaseorder = "idpurchaseorder";
            string statustype = "statustype";
            string dtfrom = "dtfrom";
            string dtthru = "dtthru";
            string author = "author";

            while (queryReader.Read())
            {
                if (addColumns)
                {
                    file.WriteLine(idstatus + "|" +
                                   idpurchaseorder + "|" +
                                   statustype + "|" +
                                   dtfrom + "|" +
                                   dtthru + "|" +
                                   author);
                    addColumns = true;
                }
                else
                {
                    string CdtFrom = queryReader["dtfrom"] == DBNull.Value ? null : Convert.ToDateTime(queryReader["dtfrom"]).ToString("yyyy-MM-dd HH:mm:ss");
                    string CdtThru = queryReader["dtthru"] == DBNull.Value ? null : Convert.ToDateTime(queryReader["dtthru"]).ToString("yyyy-MM-dd HH:mm:ss");
                    //file.WriteLine(queryReader["idpurchasing"].ToString() + "|" + queryReader["nopurchasing"].ToString());
                    file.WriteLine(queryReader["idstatus"].ToString() + "|" +
                                    queryReader["idpurchaseorder"].ToString() + "|" +
                                    queryReader["statustype"].ToString() + "|" +
                                    CdtFrom + "|" +
                                    CdtThru + "|" +
                                    queryReader["author"].ToString());
                }
            }

            queryReader.Close();
            con.Close();
            file.Close();
        }


        private void btnSimpan_Click(object sender, EventArgs e)
        {
            SaveFileDialog oSaveFileDialog = new SaveFileDialog();
            oSaveFileDialog.Filter = "txt files (*.txt) | *.txt";
            if (oSaveFileDialog.ShowDialog() == DialogResult.OK)
            {
                string fileName = oSaveFileDialog.FileName;
                string extesion = Path.GetExtension(fileName);
                string fullname = Path.GetFileName(fileName);
                txtPurchaseOrders.Text = fileName;
                pathPurchaseOrderItem= Path.GetDirectoryName(fileName) + "\\PurchaseOrderItem.txt";
                pathPurchaseOrdersstatus = Path.GetDirectoryName(fileName) + "\\purchaseOrdersStatus.txt";
            }
        }
    }
}
