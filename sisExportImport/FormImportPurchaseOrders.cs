﻿using Newtonsoft.Json;
using sisExportImport.IMPORTEKSPORTAPI.MODELS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sisExportImport
{
    public partial class FormImportPurchaseOrders : Form
    {
        public FormImportPurchaseOrders()
        {
            InitializeComponent();
        }

        string pathPurchaseOrderItem = "";
        string pathPurchaseordersStatus = "";
        //private static T _download_serialized_json_data<T>(string url) where T : new()
        //{
        //    using (var w = new WebClient())
        //    {
        //        var json_data = string.Empty;
        //        // attempt to download JSON data as a string
        //        try
        //        {
        //            json_data = w.DownloadString(url);
        //        }
        //        catch (Exception) { }
        //        // if string with JSON data is not empty, deserialize it to class and return its instance 
        //        return !string.IsNullOrEmpty(json_data) ? JsonConvert.DeserializeObject<T>(json_data) : new T();
        //    }
        //}

        //public string webPostMethod(string postData, string URL)
        //{
        //    string responseFromServer = "";
        //    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
        //    request.Method = "POST";
        //    request.Credentials = CredentialCache.DefaultCredentials;
        //    ((HttpWebRequest)request).UserAgent =
        //                      "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 7.1; Trident/5.0)";
        //    request.Accept = "/";
        //    request.UseDefaultCredentials = true;
        //    request.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
        //    byte[] byteArray = Encoding.UTF8.GetBytes(postData);
        //    request.ContentType = "application/json";
        //    request.ContentLength = byteArray.Length;
        //    Stream dataStream = request.GetRequestStream();
        //    dataStream.Write(byteArray, 0, byteArray.Length);
        //    dataStream.Close();

        //    WebResponse response = request.GetResponse();
        //    dataStream = response.GetResponseStream();
        //    StreamReader reader = new StreamReader(dataStream);
        //    responseFromServer = reader.ReadToEnd();
        //    reader.Close();
        //    dataStream.Close();
        //    response.Close();
        //    return responseFromServer;
        //}

        private void button3_Click(object sender, EventArgs e)
        {
            //using (StreamReader r = new StreamReader(@"D:\path.txt"))
            //{
            //    string json = r.ReadToEnd();
            //    List<purchase_orders> items = JsonConvert.DeserializeObject<List<purchase_orders>>(json);
            //    string postData = json;
            //    string URL = "http://localhost:3908/api/import/pp";
            //    var data = webPostMethod(postData, URL);
            //    //foreach(var item in items)
            //    //{
            //    //    Console.WriteLine(item);
            //    //}
            //    //dataGridView1.DataSource = items;
            //    //MessageBox.Show("SELESEAI IMPORT");

            //}
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //List<purchase_orders> pps =  _download_serialized_json_data<List<purchase_orders>>("http://localhost:3908/api/ekspor/pp");
            //foreach (var pp in pps) {
            //    //if (pp.content != null)
            //    //{
            //    //    Console.WriteLine(BitConverter.ToString(pp.content).Replace("-", string.Empty).ToLower());
            //    //}
            //}
            //string json = JsonConvert.SerializeObject(pps.ToArray());
            //File.WriteAllText(@"D:\path.txt", json);

            importPurchaseOrders();
            importPurchaseOrderItem();
            importPurchaseOrdersStatus();
            MessageBox.Show("SELESEAI IMPORT");
        }

        public void importPurchaseOrders()
        {
            string line;
            dbConnection dbCon = new dbConnection();
            SqlConnection db = new SqlConnection(dbCon.GetConnectionString());
            SqlTransaction transaction;
            StreamReader file = new StreamReader(txtPurchaseOrders.Text);
            try
            {
                while ((line = file.ReadLine()) != null)
                {
                    db.Open();
                    //transaction = db.BeginTransaction();
                    string[] fields = line.Split('|');
                    SqlCommand check_User_Name = new SqlCommand("select * from purchase_orders where nopurchaseorder = @no", db);
                    check_User_Name.Parameters.AddWithValue("@no", fields[1].ToString());
                    SqlDataReader reader = check_User_Name.ExecuteReader();

                    if (reader.HasRows)
                    {
                        //User Exists
                    }
                    else
                    {
                        db.Close();

                        db.Open();
                        byte[] content = Encoding.UTF8.GetBytes(fields[19].ToString());
                        SqlCommand cmd = new SqlCommand("INSERT INTO [purchase_orders]([idpurchaseorder],[nopurchaseorder],[dtorder],[divition],[sifat],[needs],[apppembelian],[dtapppembelian],[apppabrik],[dtapppabrik],[appdept],[dtappdept],[createby],[dtcreateby],[tipe],[no_bbm],[ket],[dtnecessity],[contenttype],[content],[diusulkan],[app1],[dtapp1],[app2],[dtapp2])" +
                      "VALUES('" + fields[0].ToString() + "','" + fields[1].ToString() + "','" + fields[2].ToString() + "','" + fields[3].ToString() + "','" + fields[4].ToString() + "','" + fields[5].ToString() + "','" + fields[6].ToString() + "','" + fields[7].ToString() + "','" + fields[8].ToString() + "','" + fields[9].ToString()+ "','" + fields[10].ToString() + "','" +fields[11].ToString()+ "','" + fields[12].ToString() + "','" + fields[13].ToString() + "','" + fields[14].ToString() + "','" + fields[15].ToString() + "','" + fields[16].ToString() + "','" + fields[17].ToString() + "','" + fields[18].ToString() + "','" + content + "','" + fields[20].ToString() + "','" + fields[21].ToString() + "','" +fields[22].ToString() + "','" + fields[23].ToString() + "','" +fields[24].ToString() + "')", db);
                        cmd.ExecuteNonQuery();
                    }
                    db.Close();
                    //transaction.Commit();
                }
            }
            catch (SqlException sqlError)
            {
                //transaction.Rollback();
                MessageBox.Show(sqlError.Message, "Error");
            }
            
        }

        public void importPurchaseOrderItem()
        {
            string line;
            dbConnection dbCon = new dbConnection();
            SqlConnection db = new SqlConnection(dbCon.GetConnectionString());
            //SqlTransaction transaction;

            
            StreamReader file = new StreamReader(pathPurchaseOrderItem);
            //transaction = db.BeginTransaction();
            try
            {
                while ((line = file.ReadLine()) != null)
                {
                    db.Open();
                    string[] fields = line.Split('|');
                    SqlCommand check_User_Name = new SqlCommand("select * from purchase_order_item where idpurorditem = @no", db);
                    check_User_Name.Parameters.AddWithValue("@no", fields[0].ToString());
                    SqlDataReader reader = check_User_Name.ExecuteReader();
                    if (reader.HasRows)
                    {
                        //User Exists
                    }
                    else
                    {
                        db.Close();
                        db.Open();
                        SqlCommand cmd = new SqlCommand("INSERT INTO [purchase_order_item]([idpurorditem],[idpurchaseorder],[codeproduk],[produkname],[qty],[unit],[dtoffer],[supplyername],[supplyerprice],[necessity],[dtnecessity],[remainingstock],[remainingpo],[dtcreate],[no_ph],[catatan],[status],[mata_uang],[qtybookingpo],[no_urut],[qty_terpenuhi])" +
                      "VALUES('" + fields[0].ToString() + "','" + fields[1].ToString() + "','" + fields[2].ToString()+ "','" + fields[3].ToString() + "','" + fields[4].ToString() + "','" + fields[5].ToString() + "','" + fields[6].ToString() + "','" + fields[7].ToString() + "','" + fields[8].ToString() + "','" + fields[9].ToString() + "','" + fields[10].ToString() + "','" + fields[11].ToString() + "','" + fields[12].ToString() + "','" + fields[13].ToString() + "','" + fields[14].ToString() + "','" + fields[15].ToString() + "','" + fields[16].ToString() + "','" + fields[17].ToString() + "','0','" + fields[19].ToString() + "','" + fields[20].ToString() +"')", db);
                        cmd.ExecuteNonQuery();
                    }
                    db.Close();
                }
                //transaction.Commit();
            }
            catch (SqlException sqlError)
            {
                //transaction.Rollback();
                MessageBox.Show(sqlError.Message, "Error");
            }
            
        }


        public void importPurchaseOrdersStatus()
        {
            string line;
            dbConnection dbCon = new dbConnection();
            SqlConnection db = new SqlConnection(dbCon.GetConnectionString());
            //SqlTransaction transaction;
            
            StreamReader file = new StreamReader(pathPurchaseordersStatus);
            //transaction = db.BeginTransaction();
            try
            {
                while ((line = file.ReadLine()) != null)
                {
                    db.Open();
                    string[] fields = line.Split('|');
                    SqlCommand check_User_Name = new SqlCommand("select * from purchase_orders_status where idstatus = @no", db);
                    check_User_Name.Parameters.AddWithValue("@no", fields[0].ToString());
                    SqlDataReader reader = check_User_Name.ExecuteReader();
                    if (reader.HasRows)
                    {
                        //User Exists
                    }
                    else
                    {
                        db.Close();
                        db.Open();
                        SqlCommand cmd = new SqlCommand("INSERT INTO [purchase_orders_status]([idstatus],[idpurchaseorder],[statustype],[dtfrom],[dtthru],[author])" +
                        "VALUES('" + fields[0].ToString() + "','" + fields[1].ToString() + "','" + fields[2].ToString() + "','" + fields[3].ToString() + "','" + fields[4].ToString() + "','" + fields[5].ToString() + "')", db);
                        cmd.ExecuteNonQuery();
                    }
                    db.Close();
                }
                //transaction.Commit();
            }
            catch (SqlException sqlError)
            {
                //transaction.Rollback();
                MessageBox.Show(sqlError.Message, "Error");
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                InitialDirectory = @"C:\",
                Title = "Browse Text Files",

                CheckFileExists = true,
                CheckPathExists = true,

                DefaultExt = "txt",
                Filter = "txt files (*.txt)|*.txt",
                FilterIndex = 2,
                RestoreDirectory = true,

                ReadOnlyChecked = true,
                ShowReadOnly = true
            };

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtPurchaseOrders.Text = openFileDialog1.FileName;
                pathPurchaseOrderItem = Path.GetDirectoryName(openFileDialog1.FileName) + "\\PurchaseOrderItem.txt";
                pathPurchaseordersStatus = Path.GetDirectoryName(openFileDialog1.FileName) + "\\PurchaseordersStatus.txt";
            }
        }
    }
}

