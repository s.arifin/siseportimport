﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sisExportImport
{
    public partial class FormExportPurchasing : Form
    {
        public FormExportPurchasing()
        {
            InitializeComponent();
        }

        string pathPurchasing = "";
        string pathPurchasingItem = "";
        string pathPurchasingStatus = "";
        string pathPurchasePurchasingItem = "";
        string pathPurchaseOrdersstatus = "";

        private void button1_Click(object sender, EventArgs e)
        {
            exportPurchasings();
            exportPurchasingDetail();
            exportPurchasingStatus();
            exportPurchasePurchasingItem();
            exportPurchaseOrdersStatus();
            MessageBox.Show("Informasi", "Export Selesai");
        }

        public void exportPurchaseOrdersStatus()
        {
            //OpenSqlConnection();
            dbConnection dbCon = new dbConnection();
            string connString = dbCon.GetConnectionString();
            SqlConnection con = new SqlConnection(connString);
            SqlCommand command = con.CreateCommand();

            command.CommandText = @"select  * from purchase_orders_status where idpurchaseorder in (
                                    select distinct e.idpurchaseorder from purchasing a
                                    join purchasing_item b on a.idpurchasing = b.idpurchasingfk
                                    join purchase_purchasing_item c on b.idpurchasingitem = c.idpurchasingitem
                                    join purchase_order_item d on c.idpurchaseorderitem = d.idpurorditem
                                    join purchase_orders e on d.idpurchaseorder = e.idpurchaseorder)";
            con.Open();
            SqlDataReader queryReader = command.ExecuteReader();
            StreamWriter file = new StreamWriter(pathPurchaseOrdersstatus);

            bool addColumns = false;
            string idstatus = "idstatus";
            string idpurchaseorder = "idpurchaseorder";
            string statustype = "statustype";
            string dtfrom = "dtfrom";
            string dtthru = "dtthru";
            string author = "author";

            while (queryReader.Read())
            {
                if (addColumns)
                {
                    file.WriteLine(idstatus + "|" +
                                   idpurchaseorder + "|" +
                                   statustype + "|" +
                                   dtfrom + "|" +
                                   dtthru + "|" +
                                   author);
                    addColumns = true;
                }
                else
                {
                    string cdtFrom = queryReader["dtfrom"] == DBNull.Value ? null : Convert.ToDateTime(queryReader["dtfrom"]).ToString("yyyy-MM-dd HH:mm:ss");
                    string CdtThru = queryReader["dtthru"] == DBNull.Value ? null : Convert.ToDateTime(queryReader["dtthru"]).ToString("yyyy-MM-dd HH:mm:ss");
                    //file.WriteLine(queryReader["idpurchasing"].ToString() + "|" + queryReader["nopurchasing"].ToString());
                    file.WriteLine(queryReader["idstatus"].ToString() + "|" +
                                    queryReader["idpurchaseorder"].ToString() + "|" +
                                    queryReader["statustype"].ToString() + "|" +
                                    cdtFrom+ "|" +
                                    CdtThru + "|" +
                                    queryReader["author"].ToString());
                }
            }

            queryReader.Close();
            con.Close();
            file.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
        }

        public object converDateTOString(DateTime? date, string format)
        {
            if (!date.HasValue)
            {
                return "null";
            }
            else
            if (date.HasValue)
            {
                // "yyyy-MM-dd HH:mm:ss"
                string date_str = date.ToString();
                DateTime dateA = Convert.ToDateTime(date_str);
                string dateB = dateA.ToString(format);
                dateB = "'" + dateB + "'";
                return dateB;
            }
            else
            {
                return "null";
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SaveFileDialog oSaveFileDialog = new SaveFileDialog();
            oSaveFileDialog.Filter = "txt files (*.txt) | *.txt";
            if (oSaveFileDialog.ShowDialog() == DialogResult.OK)
            {
                string fileName = oSaveFileDialog.FileName;
                string extesion = Path.GetExtension(fileName);
                string fullname = Path.GetFileName(fileName);
                txtPurchasing.Text = fileName;
                pathPurchasingItem = Path.GetDirectoryName(fileName) + "\\PurchasingItem.txt";
                pathPurchasingStatus = Path.GetDirectoryName(fileName) + "\\purchasingStatus.txt";
                pathPurchasePurchasingItem = Path.GetDirectoryName(fileName) + "\\purchasePurchasingItems.txt";
                pathPurchaseOrdersstatus = Path.GetDirectoryName(fileName) + "\\purchaseOrdersStatus.txt";
            }
        }
        public void exportPurchasings()
        {
            //OpenSqlConnection();
            dbConnection dbCon = new dbConnection();
            string connString = dbCon.GetConnectionString();
            SqlConnection con = new SqlConnection(connString);
            SqlCommand command = con.CreateCommand();

            command.CommandText = "Select * from purchasing";
            con.Open();
            SqlDataReader queryReader = command.ExecuteReader();
            StreamWriter file = new StreamWriter(txtPurchasing.Text);

            bool addColumns = false;
            string idpurchasing = "idpurchasing";
            string nopurchasing = "nopurchasing";
            string supliercode = "supliercode";
            string sentto = "sentto";
            string paymentterms = "paymentterms";
            string revision = "revision";
            string tenor = "tenor";
            string dtcreated = "dtcreated";
            string createdby = "createdby";
            string modifiedby = "modifiedby";
            string dtmodified = "dtmodified";
            string valuta = "valuta";
            string tolerancenumsend = "tolerancenumsend";
            string totalprice = "totalprice";
            string isdisc = "isdisc";
            string disc = "disc";
            string isppn = "isppn";
            string ppn = "ppn";
            string totaldisc = "totaldisc";
            string notes = "notes";
            string printcount = "printcount";
            string otorisasi = "otorisasi";
            string price = "price";
            string notes2 = "notes2";
            string otorisasi2 = "otorisasi2";
            string div = "div";
            string paynotes = "paynotes";

            while (queryReader.Read())
            {
                if (addColumns)
                {
                    file.WriteLine(idpurchasing + "|" +
                                    nopurchasing + "|" +
                                    supliercode + "|" +
                                    sentto + "|" +
                                    paymentterms + "|" +
                                    revision + "|" +
                                    tenor + "|" +
                                    dtcreated + "|" +
                                    createdby + "|" +
                                    modifiedby + "|" +
                                    dtmodified + "|" +
                                    valuta + "|" +
                                    tolerancenumsend + "|" +
                                    totalprice + "|" +
                                    isdisc + "|" +
                                    disc + "|" +
                                    isppn + "|" +
                                    ppn + "|" +
                                    totaldisc + "|" +
                                    notes + "|" +
                                    printcount + "|" +
                                    otorisasi + "|" +
                                    price + "|" +
                                    notes2 + "|" +
                                    otorisasi2 + "|" +
                                    div + "|" +
                                    paynotes);
                    addColumns = true;
                }
                else
                {
                    string Crevision = queryReader["revision"] == DBNull.Value ? "0" : queryReader["revision"].ToString();
                    string Ctenor = queryReader["tenor"] == DBNull.Value ? "0" : queryReader["tenor"].ToString();
                    string CdtCreated =  queryReader["dtcreated"] == DBNull.Value ? null : Convert.ToDateTime(queryReader["dtcreated"]).ToString("yyyy-MM-dd HH:mm:ss");
                    string cdtModified =  queryReader["dtmodified"] == DBNull.Value ? null : Convert.ToDateTime(queryReader["dtmodified"]).ToString("yyyy-MM-dd HH:mm:ss");
                    //file.WriteLine(queryReader["idpurchasing"].ToString() + "|" + queryReader["nopurchasing"].ToString());
                    file.WriteLine(queryReader["idpurchasing"].ToString() + "|" +
                                    queryReader["nopurchasing"].ToString() + "|" +
                                    queryReader["supliercode"].ToString() + "|" +
                                    queryReader["sentto"].ToString() + "|" +
                                    queryReader["paymentterms"].ToString() + "|" +
                                     Crevision + "|" +
                                     Ctenor + "|" +
                                    CdtCreated + "|" +
                                    queryReader["createdby"].ToString() + "|" +
                                    queryReader["modifiedby"].ToString() + "|" +
                                    cdtModified + "|" +
                                    queryReader["valuta"].ToString() + "|" +
                                    queryReader["tolerancenumsend"].ToString() + "|" +
                                    queryReader["totalprice"].ToString() + "|" +
                                    queryReader["isdisc"].ToString() + "|" +
                                    queryReader["disc"].ToString() + "|" +
                                    queryReader["isppn"].ToString() + "|" +
                                    queryReader["ppn"].ToString() + "|" +
                                    queryReader["totaldisc"].ToString() + "|" +
                                    queryReader["notes"].ToString() + "|" +
                                    queryReader["printcount"].ToString() + "|" +
                                    queryReader["otorisasi"].ToString() + "|" +
                                    queryReader["price"].ToString() + "|" +
                                    queryReader["notes2"].ToString() + "|" +
                                    queryReader["otorisasi2"].ToString() + "|" +
                                    queryReader["div"].ToString() + "|" +
                                    queryReader["paynotes"].ToString());
                }
            }

            queryReader.Close();
            con.Close();
            file.Close();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        public void exportPurchasingDetail()
        {
            //OpenSqlConnection();
            dbConnection dbCon = new dbConnection();
            string connString = dbCon.GetConnectionString();
            SqlConnection con = new SqlConnection(connString);
            SqlCommand command = con.CreateCommand();

            command.CommandText = "Select * from purchasing_item";
            con.Open();
            SqlDataReader queryReader = command.ExecuteReader();
            StreamWriter file = new StreamWriter(pathPurchasingItem);

            bool addColumns = false;
            string idpurchasingitem = "idpurchasingitem";
            string idpurchasingfk = "idpurchasingfk";
            string productcode = "productcode";
            string productname = "productname";
            string qty = "qty";
            string ppqty = "ppqty";
            string price = "price";
            string bidprice = "bidprice";
            string totalprice = "totalprice";
            string dtsent = "dtsent";
            string divitions = "divitions";
            string uom = "uom";
            string qty_kirim = "";

            while (queryReader.Read())
            {
                if (addColumns)
                {
                    file.WriteLine(idpurchasingitem + "|" +
                                    idpurchasingfk + "|" +
                                    productcode + "|" +
                                    productname + "|" +
                                    qty + "|" +
                                    ppqty + "|" +
                                    price + "|" +
                                    bidprice + "|" +
                                    totalprice + "|" +
                                    dtsent + "|" +
                                    divitions + "|" +
                                    uom + "|" +
                                    qty_kirim);
                    addColumns = true;
                }
                else
                {
                    string CdtSent = queryReader["dtsent"] == DBNull.Value ? null : Convert.ToDateTime(queryReader["dtsent"]).ToString("yyyy-MM-dd HH:mm:ss");
                    //file.WriteLine(queryReader["idpurchasing"].ToString() + "|" + queryReader["nopurchasing"].ToString());
                    file.WriteLine(queryReader["idpurchasingitem"].ToString() + "|" +
                                     queryReader["idpurchasingfk"].ToString() + "|" +
                                     queryReader["productcode"].ToString() + "|" +
                                     queryReader["productname"].ToString() + "|" +
                                     queryReader["qty"].ToString() + "|" +
                                     queryReader["ppqty"].ToString() + "|" +
                                     queryReader["price"].ToString() + "|" +
                                     queryReader["bidprice"].ToString() + "|" +
                                     queryReader["totalprice"].ToString() + "|" +
                                     CdtSent + "|" +
                                     queryReader["divitions"].ToString() + "|" +
                                     queryReader["uom"].ToString() + "|" +
                                     queryReader["qty_kirim"].ToString());
                }
            }

            queryReader.Close();
            con.Close();
            file.Close();
        }

        public void exportPurchasingStatus()
        {
            //OpenSqlConnection();
            dbConnection dbCon = new dbConnection();
            string connString = dbCon.GetConnectionString();
            SqlConnection con = new SqlConnection(connString);
            SqlCommand command = con.CreateCommand();

            command.CommandText = "Select * from purchasing_status";
            con.Open();
            SqlDataReader queryReader = command.ExecuteReader();
            StreamWriter file = new StreamWriter(pathPurchasingStatus);

            bool addColumns = false;
            string idstatus = "idstatus";
            string idpurchasing = "idpurchasing";
            string statustype = "statustype";
            string dtfrom = "dtfrom";
            string dtthru = "dtthru";
            string author = "author";

            while (queryReader.Read())
            {
                if (addColumns)
                {
                    file.WriteLine(idstatus + "|" +
                                   idpurchasing + "|" +
                                   statustype + "|" +
                                   dtfrom + "|" +
                                   dtthru + "|" +
                                   author);
                    addColumns = true;
                }
                else
                {
                    string cdtFrom = queryReader["dtfrom"] == DBNull.Value ? null : Convert.ToDateTime(queryReader["dtfrom"]).ToString("yyyy-MM-dd HH:mm:ss");
                    string CdtThru = queryReader["dtthru"] == DBNull.Value ? null : Convert.ToDateTime(queryReader["dtthru"]).ToString("yyyy-MM-dd HH:mm:ss");
                    file.WriteLine(queryReader["idstatus"].ToString() + "|" +
                                    queryReader["idpurchasing"].ToString() + "|" +
                                    queryReader["statustype"].ToString() + "|" +
                                    cdtFrom + "|" +
                                    CdtThru + "|" +
                                    queryReader["author"].ToString());
                }
            }

            queryReader.Close();
            con.Close();
            file.Close();
        }

        public void exportPurchasePurchasingItem()
        {
            //OpenSqlConnection();
            dbConnection dbCon = new dbConnection();
            string connString = dbCon.GetConnectionString();
            SqlConnection con = new SqlConnection(connString);
            SqlCommand command = con.CreateCommand();

            command.CommandText = "Select * from purchase_purchasing_item";
            con.Open();
            SqlDataReader queryReader = command.ExecuteReader();
            StreamWriter file = new StreamWriter(pathPurchasePurchasingItem);

            bool addColumns = false;
            string idpurchasepurchasing = "idpurchasepurchasing";
            string idpurchaseorderitem = "idpurchaseorderitem";
            string idpurchasingitem = "idpurchasingitem";

            while (queryReader.Read())
            {
                if (addColumns)
                {
                    file.WriteLine(idpurchasepurchasing + "|" +
                                    idpurchaseorderitem + "|" +
                                    idpurchasingitem);
                    addColumns = true;
                }
                else
                {
                    //file.WriteLine(queryReader["idpurchasing"].ToString() + "|" + queryReader["nopurchasing"].ToString());
                    file.WriteLine(queryReader["idpurchasepurchasing"].ToString() + "|" +
                                    queryReader["idpurchaseorderitem"].ToString() + "|" +
                                    queryReader["idpurchasingitem"].ToString());
                }
            }

            queryReader.Close();
            con.Close();
            file.Close();
        }

        public void importPurchasing()
        {
            string line;
            dbConnection dbCon = new dbConnection();            
            using (SqlConnection con = new SqlConnection(dbCon.GetConnectionString()))
            {
                con.Open();
                using (StreamReader file = new StreamReader(@"D:\purchasing.txt"))
                {

                    try
                    {
                        while ((line = file.ReadLine()) != null)
                        {
                            string[] fields = line.Split(';');
                            SqlCommand cmd = new SqlCommand("INSERT INTO [test_table]([idpurchasing],[nopurchasing],[supliercode],[sentto],[paymentterms],[revision],[tenor],[dtcreated],[createdby],[modifiedby],[dtmodified],[valuta],[tolerancenumsend],[totalprice],[isdisc],[disc],[isppn],[ppn],[totaldisc],[notes],[printcount],[otorisasi],[price],[notes2],[otorisasi2],[div],[paynotes])VALUES(@idpurchasing,@nopurchasing,@supliercode,@sentto,@paymentterms,@revision,@tenor,@dtcreated,@createdby,@modifiedby,@dtmodified,@valuta,@tolerancenumsend,@totalprice,@isdisc,@disc,@isppn,@ppn,@totaldisc,@notes,@printcount,@otorisasi,@price,@notes2,@otorisasi2,@div,@paynotes)", con);
                            cmd.Parameters.AddWithValue("@idpurchasing", fields[0].ToString());
                            cmd.Parameters.AddWithValue("@nopurchasing", fields[1].ToString());
                            cmd.Parameters.AddWithValue("@supliercode", fields[2].ToString());
                            cmd.Parameters.AddWithValue("@sentto", fields[3].ToString());
                            cmd.Parameters.AddWithValue("@paymentterms", fields[4].ToString());
                            cmd.Parameters.AddWithValue("@revision", fields[5]);
                            cmd.Parameters.AddWithValue("@tenor", fields[6]);
                            cmd.Parameters.AddWithValue("@dtcreated", Convert.ToDateTime(fields[7].ToString()).ToString("yyyy-MM-dd HH:mm:ss"));
                            cmd.Parameters.AddWithValue("@createdby", fields[8].ToString());
                            cmd.Parameters.AddWithValue("@modifiedby", fields[9].ToString());
                            cmd.Parameters.AddWithValue("@dtmodified", Convert.ToDateTime(fields[10].ToString()).ToString("yyyy-MM-dd HH:mm:ss"));
                            cmd.Parameters.AddWithValue("@valuta", fields[11].ToString());
                            cmd.Parameters.AddWithValue("@tolerancenumsend", fields[12]);
                            cmd.Parameters.AddWithValue("@totalprice", fields[13]);
                            cmd.Parameters.AddWithValue("@isdisc", fields[14]);
                            cmd.Parameters.AddWithValue("@disc", fields[15]);
                            cmd.Parameters.AddWithValue("@isppn", fields[16]);
                            cmd.Parameters.AddWithValue("@ppn", fields[17].ToString());
                            cmd.Parameters.AddWithValue("@totaldisc", fields[18]);
                            cmd.Parameters.AddWithValue("@notes", fields[19].ToString());
                            cmd.Parameters.AddWithValue("@printcount", fields[20]);
                            cmd.Parameters.AddWithValue("@otorisasi", fields[21].ToString());
                            cmd.Parameters.AddWithValue("@price", fields[22]);
                            cmd.Parameters.AddWithValue("@notes2", fields[23].ToString());
                            cmd.Parameters.AddWithValue("@otorisasi2", fields[24].ToString());
                            cmd.Parameters.AddWithValue("@div", fields[25].ToString());
                            cmd.Parameters.AddWithValue("@paynotes", fields[26].ToString());
                            cmd.ExecuteNonQuery();
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                }
            }
        }
    }
}


