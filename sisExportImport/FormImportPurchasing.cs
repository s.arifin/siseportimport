﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sisExportImport
{
    public partial class FormImportPurchasing : Form
    {
        public FormImportPurchasing()
        {
            InitializeComponent();
        }
        string pathPurchasingItem = "";
        string pathPurchasingStatus = "";
        string pathPurchasePurchasingItem = "";
        string pathPurchaseOrdersStatus = "";

        //static private string GetConnectionString()
        //{
        //    // To avoid storing the connection string in your code,
        //    // you can retrieve it from a configuration file.
        //    return "Data Source=localhost;Initial Catalog=sbsserver;"
        //        + "Integrated Security=true;";
        //}

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                InitialDirectory = @"C:\",
                Title = "Browse Text Files",

                CheckFileExists = true,
                CheckPathExists = true,

                DefaultExt = "txt",
                Filter = "txt files (*.txt)|*.txt",
                FilterIndex = 2,
                RestoreDirectory = true,

                ReadOnlyChecked = true,
                ShowReadOnly = true
            };

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtPurchasing.Text = openFileDialog1.FileName;
                pathPurchasingItem = Path.GetDirectoryName(openFileDialog1.FileName) + "\\PurchasingItem.txt";
                pathPurchasingStatus = Path.GetDirectoryName(openFileDialog1.FileName) + "\\purchasingStatus.txt";
                pathPurchasePurchasingItem = Path.GetDirectoryName(openFileDialog1.FileName) + "\\purchasePurchasingItems.txt";
                pathPurchaseOrdersStatus = Path.GetDirectoryName(openFileDialog1.FileName) + "\\purchaseOrdersStatus.txt";

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            importPurchasingNew();
            importPurchasingItems();
            importPurchasingStatus();
            importPurchasePurchasingItem();
            importPurchaseOrdersStatus();
            MessageBox.Show("Informasi", "Import Selesai");
        }
        public void importPurchasingNew()
        {
            string line;
            dbConnection dbCon = new dbConnection();
            string connString = dbCon.GetConnectionString();
            SqlConnection db = new SqlConnection(connString);
            //SqlTransaction transaction;

            
            StreamReader file = new StreamReader(txtPurchasing.Text);
            //transaction = db.BeginTransaction();
            //try
            //{
                db.Open();
                SqlCommand del = new SqlCommand("delete purchasing", db);
                del.ExecuteNonQuery();
                db.Close();
                while ((line = file.ReadLine()) != null)
                {
                    db.Open();
                    string[] fields = line.Split('|');
                    SqlCommand cmd = new SqlCommand("INSERT INTO purchasing([idpurchasing],[nopurchasing],[supliercode],[sentto],[paymentterms],[revision],[tenor],[dtcreated],[createdby],[modifiedby],[dtmodified],[valuta],[tolerancenumsend],[totalprice],[isdisc],[disc],[isppn],[ppn],[totaldisc],[notes],[printcount],[otorisasi],[price],[notes2],[otorisasi2],[div],[paynotes])" +
                    "VALUES('" + fields[0].ToString() + "','" + fields[1].ToString() + "','" + fields[2].ToString() + "','" + fields[3].ToString() + "','" + fields[4].ToString() + "','" + fields[5] + "','" + fields[6] + "','" + fields[7].ToString() + "','" + fields[8].ToString() + "','" + fields[9].ToString() + "','" + fields[10].ToString() + "','" + fields[11].ToString() + "','" + fields[12] + "','" + fields[13] + "','" + fields[14] + "','" + fields[15] + "','" + fields[16] + "','" + fields[17].ToString() + "','" + fields[18] + "','" + fields[19].ToString() + "','" + fields[20] + "','" + fields[21].ToString() + "','" + fields[22] + "','" + fields[23].ToString() + "','" + fields[24].ToString() + "','" + fields[25].ToString() + "','" + fields[26].ToString() + "')", db);
                    cmd.ExecuteNonQuery();
                    db.Close();
                }
                //transaction.Commit();
            //}
            //catch (SqlException sqlError)
            //{
            //    //transaction.Rollback();
            //    MessageBox.Show(sqlError.Message, "Error");
            //}
        }

        public void importPurchasingItems()
        {
            string line;
            dbConnection dbCon = new dbConnection();
            string connString = dbCon.GetConnectionString();
            SqlConnection db = new SqlConnection(connString);
            //SqlTransaction transaction;

            StreamReader file = new StreamReader(pathPurchasingItem);
            //transaction = db.BeginTransaction();
            //try
            //{
                db.Open();
                SqlCommand del = new SqlCommand("delete purchasing_item", db);
                del.ExecuteNonQuery();
                db.Close();
                while ((line = file.ReadLine()) != null)
                {
                    db.Open();
                    string[] fields = line.Split('|');
                    SqlCommand cmd = new SqlCommand("INSERT INTO [purchasing_item]([idpurchasingitem],[idpurchasingfk],[productcode],[productname],[qty],[ppqty],[price],[bidprice],[totalprice],[dtsent],[divitions],[uom],[qty_kirim])" +
                    "VALUES('" + fields[0].ToString() + "','" + fields[1].ToString() + "','" + fields[2].ToString() + "','" + fields[3].ToString() + "','" + fields[4].ToString() + "','" + fields[5] + "','" + fields[6] + "','" + fields[7].ToString() + "','" + fields[8].ToString() + "','" + fields[9].ToString() + "','" + fields[10].ToString() + "','" + fields[11].ToString() + "','" + fields[12].ToString() + "')", db);
                    cmd.ExecuteNonQuery();
                    db.Close();
                }
                //transaction.Commit();
            //}
            //catch (SqlException sqlError)
            //{
            //    //transaction.Rollback();
            //    MessageBox.Show(sqlError.Message, "Error");
            //}
            db.Close();
        }

        public void importPurchasingStatus()
        {
            string line;
            dbConnection dbCon = new dbConnection();
            string connString = dbCon.GetConnectionString();
            SqlConnection db = new SqlConnection(connString);
            //SqlTransaction transaction;

            StreamReader file = new StreamReader(pathPurchasingStatus);
            //transaction = db.BeginTransaction();
            //try
            //{
                db.Open();
                SqlCommand del = new SqlCommand("delete purchasing_status", db);
                del.ExecuteNonQuery();
                db.Close();
                while ((line = file.ReadLine()) != null)
                {
                    db.Open();
                    string[] fields = line.Split('|');
                    SqlCommand cmd = new SqlCommand("INSERT INTO [purchasing_status]([idstatus],[idpurchasing],[statustype],[dtfrom],[dtthru],[author])" +
                    "VALUES('" + fields[0].ToString() + "','" + fields[1].ToString() + "','" + fields[2].ToString() + "','" + fields[3].ToString() + "','" + fields[4].ToString() + "','" + fields[5].ToString() + "')", db);
                    cmd.ExecuteNonQuery();
                    db.Close();
                }
                //transaction.Commit();
            //}
            //catch (SqlException sqlError)
            //{
            //    //transaction.Rollback();
            //    MessageBox.Show(sqlError.Message, "Error");
            //}
            db.Close();
        }

        public void importPurchasePurchasingItem()
        {
            string line;
            dbConnection dbCon = new dbConnection();
            string connString = dbCon.GetConnectionString();
            SqlConnection db = new SqlConnection(connString);
            //SqlTransaction transaction;
            
            StreamReader file = new StreamReader(pathPurchasePurchasingItem);
            //transaction = db.BeginTransaction();
            //try
            //{
                db.Open();
                SqlCommand del = new SqlCommand("delete purchase_purchasing_item", db);
                del.ExecuteNonQuery();
                db.Close();
                while ((line = file.ReadLine()) != null)
                {
                    db.Open();
                    string[] fields = line.Split('|');
                    SqlCommand cmd = new SqlCommand("INSERT INTO [purchase_purchasing_item]([idpurchasepurchasing],[idpurchaseorderitem],[idpurchasingitem])" +
                    "VALUES('" + fields[0].ToString() + "','" + fields[1].ToString() + "','" + fields[2].ToString()+ "')", db);
                    cmd.ExecuteNonQuery();
                    db.Close();
                }
                //transaction.Commit();
            //}
            //catch (SqlException sqlError)
            //{
            //    //transaction.Rollback();
            //    MessageBox.Show(sqlError.Message, "Error");
            //}            
        }

        public void importPurchaseOrdersStatus()
        {
            string line;
            dbConnection dbCon = new dbConnection();
            SqlConnection db = new SqlConnection(dbCon.GetConnectionString());
            //SqlTransaction transaction;
            
            StreamReader file = new StreamReader(pathPurchaseOrdersStatus);
            //transaction = db.BeginTransaction();
            //try
            //{
                while ((line = file.ReadLine()) != null)
                {
                    db.Open();
                    string[] fields = line.Split('|');
                    SqlCommand check_User_Name = new SqlCommand("select * from purchase_orders_status where dtfrom < GETDATE() and GETDATE() < dtthru and idstatus = @no and statustype = 21", db);
                    check_User_Name.Parameters.AddWithValue("@no", fields[0].ToString());
                    SqlDataReader reader = check_User_Name.ExecuteReader();

                    if (reader.HasRows)
                    {
                        db.Close();
                        db.Open();
                        SqlCommand cmd = new SqlCommand("update purchase_orders_status set dtthru = @dtthru where idstatus =  @no", db);
                        cmd.Parameters.AddWithValue("@no", fields[0].ToString());
                        cmd.Parameters.AddWithValue("@dtthru", fields[4].ToString());
                        cmd.ExecuteNonQuery();
                        db.Close();
                    }
                    else
                    {

                        db.Close();
                        db.Open();
                        SqlCommand checksts = new SqlCommand("select * from purchase_orders_status where idstatus = @no", db);
                        checksts.Parameters.AddWithValue("@no", fields[0].ToString());
                        SqlDataReader readera = checksts.ExecuteReader();
                        if (readera.HasRows)
                        {

                        }
                        else {
                            db.Close();
                            db.Open();
                            SqlCommand cmd = new SqlCommand("INSERT INTO [purchase_orders_status]([idstatus],[idpurchaseorder],[statustype],[dtfrom],[dtthru],[author])" +
                            "VALUES('" + fields[0].ToString() + "','" + fields[1].ToString() + "','" + fields[2].ToString() + "','" + fields[3].ToString() + "','" + fields[4].ToString() + "','" + fields[5].ToString() + "')", db);
                            cmd.ExecuteNonQuery();
                            db.Close();
                        }
                    }

                    db.Close();
                }
            //}
            //catch (SqlException sqlError)
            //{
            //    //transaction.Rollback();
            //    MessageBox.Show(sqlError.Message, "Error");
            //}
            db.Close();
        }

        private void FormImportPurchasing_Load(object sender, EventArgs e)
        {

        }
    }
}
