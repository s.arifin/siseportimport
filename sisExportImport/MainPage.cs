﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sisExportImport
{
    public partial class MainPage : Form
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void importPurchasingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormImportPurchasing childForm = new FormImportPurchasing();
            childForm.MdiParent = this;
            childForm.Show();
            childForm.WindowState = FormWindowState.Maximized;
        }

        private void exportPurchasingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormExportPurchasing childForm = new FormExportPurchasing();
            childForm.MdiParent = this;
            childForm.Show();
            childForm.WindowState = FormWindowState.Maximized;
        }

        private void exportPurchaseOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormExportPurchaseOrders childForm = new FormExportPurchaseOrders();
            childForm.MdiParent = this;
            childForm.Show();
            childForm.WindowState = FormWindowState.Maximized;
        }

        private void importPurchaseOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormImportPurchaseOrders childForm = new FormImportPurchaseOrders();
            childForm.MdiParent = this;
            childForm.Show();
            childForm.WindowState = FormWindowState.Maximized;
        }
    }
}
